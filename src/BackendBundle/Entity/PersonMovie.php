<?php
/**
 * Created by Roman Bas.
 * User: roman
 * Date: 09/06/2018
 * Time: 18:48
 */

namespace BackendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="movie_person")
 */
class PersonMovie{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=35)
     */
    private $job;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $character = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $order = null;

    /**
     * @ORM\ManyToOne(targetEntity="BackendBundle\Entity\Person", inversedBy="person_movie")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity="BackendBundle\Entity\Movie", inversedBy="person_movie")
     * @ORM\JoinColumn(name="movie_id", referencedColumnName="id")
     */
    private $movie;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set person
     *
     * @param \BackendBundle\Entity\Person $person
     *
     * @return PersonMovie
     */
    public function setPerson(Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \BackendBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set movie
     *
     * @param \BackendBundle\Entity\Movie $movie
     *
     * @return PersonMovie
     */
    public function setMovie(Movie $movie = null)
    {
        $this->movie = $movie;

        return $this;
    }

    /**
     * Get movie
     *
     * @return \BackendBundle\Entity\Movie
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * Add movieNomination
     *
     * @param \BackendBundle\Entity\MovieNomination $movieNomination
     *
     * @return PersonMovie
     */
    public function addMovieNomination(MovieNomination $movieNomination)
    {
        $this->movie_nomination[] = $movieNomination;

        return $this;
    }

    /**
     * Remove movieNomination
     *
     * @param \BackendBundle\Entity\MovieNomination $movieNomination
     */
    public function removeMovieNomination(MovieNomination $movieNomination)
    {
        $this->movie_nomination->removeElement($movieNomination);
    }

    /**
     * Get movieNomination
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovieNomination()
    {
        return $this->movie_nomination;
    }

    /**
     * @return mixed
     */
    public function getCharacter()
    {
        return $this->character;
    }

    /**
     * @param mixed $character
     */
    public function setCharacter($character)
    {
        $this->character = $character;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param mixed $job
     */
    public function setJob($job)
    {
        $this->job = $job;
    }

}
