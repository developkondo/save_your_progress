<?php
/**
 * Created by Roman Bas.
 * User: romabas
 * Date: 13/08/18
 * Time: 11:21
 */

namespace BackendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="tv_serie")
 */
class TvSerie{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $original_title;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $main_title;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $rating_tmdb_avg = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rating_tmdb_count = null;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $id_tmdb = null;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $backdrop_path = null;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $poster_path = null;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $status = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $popularity = null;

    /**
     * @ORM\Column(type="text")
     */
    private $synopsis;

    /**
     * @ORM\Column(type="date")
     */
    private $premiere_date;

    /**
     * @ORM\Column(type="integer")
     */
    private $episode_duration;

    /**
     * @ORM\ManyToMany(targetEntity="BackendBundle\Entity\Country")
     * @ORM\JoinColumn(name="tv_serie_country")
     */
    private $countries;

    /**
     * @ORM\ManyToMany(targetEntity="BackendBundle\Entity\Genre")
     * @ORM\JoinTable(name="tv_serie_genre")
     */
    private $genres;

    /**
     * @ORM\ManyToMany(targetEntity="BackendBundle\Entity\Producer")
     * @ORM\JoinTable(name="tv_serie_producer")
     */
    private $producers;

    /**
     * @ORM\OneToMany(targetEntity="BackendBundle\Entity\Season", mappedBy="tv_series")
     */
    private $seasons;

    public function __construct(){
        $this->seasons = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set originalTitle
     *
     * @param string $originalTitle
     *
     * @return TvSerie
     */
    public function setOriginalTitle($originalTitle)
    {
        $this->original_title = $originalTitle;

        return $this;
    }

    /**
     * Get originalTitle
     *
     * @return string
     */
    public function getOriginalTitle()
    {
        return $this->original_title;
    }

    /**
     * Set mainTitle
     *
     * @param string $mainTitle
     *
     * @return TvSerie
     */
    public function setMainTitle($mainTitle)
    {
        $this->main_title = $mainTitle;

        return $this;
    }

    /**
     * Get mainTitle
     *
     * @return string
     */
    public function getMainTitle()
    {
        return $this->main_title;
    }

    /**
     * Set ratingTmdbAvg
     *
     * @param float $ratingTmdbAvg
     *
     * @return TvSerie
     */
    public function setRatingTmdbAvg($ratingTmdbAvg)
    {
        $this->rating_tmdb_avg = $ratingTmdbAvg;

        return $this;
    }

    /**
     * Get ratingTmdbAvg
     *
     * @return float
     */
    public function getRatingTmdbAvg()
    {
        return $this->rating_tmdb_avg;
    }

    /**
     * Set ratingTmdbCount
     *
     * @param integer $ratingTmdbCount
     *
     * @return TvSerie
     */
    public function setRatingTmdbCount($ratingTmdbCount)
    {
        $this->rating_tmdb_count = $ratingTmdbCount;

        return $this;
    }

    /**
     * Get ratingTmdbCount
     *
     * @return integer
     */
    public function getRatingTmdbCount()
    {
        return $this->rating_tmdb_count;
    }

    /**
     * Set idTmdb
     *
     * @param integer $idTmdb
     *
     * @return TvSerie
     */
    public function setIdTmdb($idTmdb)
    {
        $this->id_tmdb = $idTmdb;

        return $this;
    }

    /**
     * Get idTmdb
     *
     * @return integer
     */
    public function getIdTmdb()
    {
        return $this->id_tmdb;
    }

    /**
     * Set backdropPath
     *
     * @param string $backdropPath
     *
     * @return TvSerie
     */
    public function setBackdropPath($backdropPath)
    {
        $this->backdrop_path = $backdropPath;

        return $this;
    }

    /**
     * Get backdropPath
     *
     * @return string
     */
    public function getBackdropPath()
    {
        return $this->backdrop_path;
    }

    /**
     * Set posterPath
     *
     * @param string $posterPath
     *
     * @return TvSerie
     */
    public function setPosterPath($posterPath)
    {
        $this->poster_path = $posterPath;

        return $this;
    }

    /**
     * Get posterPath
     *
     * @return string
     */
    public function getPosterPath()
    {
        return $this->poster_path;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TvSerie
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set popularity
     *
     * @param float $popularity
     *
     * @return TvSerie
     */
    public function setPopularity($popularity)
    {
        $this->popularity = $popularity;

        return $this;
    }

    /**
     * Get popularity
     *
     * @return float
     */
    public function getPopularity()
    {
        return $this->popularity;
    }

    /**
     * Set synopsis
     *
     * @param string $synopsis
     *
     * @return TvSerie
     */
    public function setSynopsis($synopsis)
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    /**
     * Get synopsis
     *
     * @return string
     */
    public function getSynopsis()
    {
        return $this->synopsis;
    }

    /**
     * Set premiereDate
     *
     * @param \DateTime $premiereDate
     *
     * @return TvSerie
     */
    public function setPremiereDate($premiereDate)
    {
        $this->premiere_date = $premiereDate;

        return $this;
    }

    /**
     * Get premiereDate
     *
     * @return \DateTime
     */
    public function getPremiereDate()
    {
        return $this->premiere_date;
    }

    /**
     * Set episodeDuration
     *
     * @param integer $episodeDuration
     *
     * @return TvSerie
     */
    public function setEpisodeDuration($episodeDuration)
    {
        $this->episode_duration = $episodeDuration;

        return $this;
    }

    /**
     * Get episodeDuration
     *
     * @return integer
     */
    public function getEpisodeDuration()
    {
        return $this->episode_duration;
    }

    /**
     * Add country
     *
     * @param \BackendBundle\Entity\Country $country
     *
     * @return TvSerie
     */
    public function addCountry(\BackendBundle\Entity\Country $country)
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param \BackendBundle\Entity\Country $country
     */
    public function removeCountry(\BackendBundle\Entity\Country $country)
    {
        $this->countries->removeElement($country);
    }

    /**
     * Get countries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Add genre
     *
     * @param \BackendBundle\Entity\Genre $genre
     *
     * @return TvSerie
     */
    public function addGenre(\BackendBundle\Entity\Genre $genre)
    {
        $this->genres[] = $genre;

        return $this;
    }

    /**
     * Remove genre
     *
     * @param \BackendBundle\Entity\Genre $genre
     */
    public function removeGenre(\BackendBundle\Entity\Genre $genre)
    {
        $this->genres->removeElement($genre);
    }

    /**
     * Get genres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * Add producer
     *
     * @param \BackendBundle\Entity\Producer $producer
     *
     * @return TvSerie
     */
    public function addProducer(\BackendBundle\Entity\Producer $producer)
    {
        $this->producers[] = $producer;

        return $this;
    }

    /**
     * Remove producer
     *
     * @param \BackendBundle\Entity\Producer $producer
     */
    public function removeProducer(\BackendBundle\Entity\Producer $producer)
    {
        $this->producers->removeElement($producer);
    }

    /**
     * Get producers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducers()
    {
        return $this->producers;
    }

    /**
     * Add season
     *
     * @param \BackendBundle\Entity\Season $season
     *
     * @return TvSerie
     */
    public function addSeason(\BackendBundle\Entity\Season $season)
    {
        $this->seasons[] = $season;

        return $this;
    }

    /**
     * Remove season
     *
     * @param \BackendBundle\Entity\Season $season
     */
    public function removeSeason(\BackendBundle\Entity\Season $season)
    {
        $this->seasons->removeElement($season);
    }

    /**
     * Get seasons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeasons()
    {
        return $this->seasons;
    }
}
