<?php
/**
 * Created by Roman Bas.
 * User: roman
 * Date: 08/06/2018
 * Time: 23:01
 */

namespace BackendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="person")
 */
class Person{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private $full_name;

    /**
     * @ORM\Column(type="integer", length=1, nullable=true)
     */
    private $gender = null;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birth_date = null;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $death_date = null;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $birth_place = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $biography = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $popularity = null;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $profile_path = null;

    /**
     * @ORM\OneToMany(targetEntity="BackendBundle\Entity\PersonMovie", mappedBy="person", cascade={"persist"})
     */
    private $person_movie;

    /**
     * @ORM\OneToMany(targetEntity="BackendBundle\Entity\PersonTvSeason", mappedBy="person", cascade={"persist"})
     */
    private $person_tv_season;

    /**
     * @ORM\OneToMany(targetEntity="BackendBundle\Entity\MovieNomination", mappedBy="person")
     */
    private $movie_nomination;

    public function __construct(){
        $this->person_movie = new ArrayCollection();
        $this->movie_nomination = new ArrayCollection();
        $this->person_tv_season = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * @param mixed $birth_date
     */
    public function setBirthDate($birth_date)
    {
        $this->birth_date = $birth_date;
    }

    /**
     * @return mixed
     */
    public function getBirthPlace()
    {
        return $this->birth_place;
    }

    /**
     * @param mixed $birth_place
     */
    public function setBirthPlace($birth_place)
    {
        $this->birth_place = $birth_place;
    }

    /**
     * @return mixed
     */
    public function getBiography()
    {
        return $this->biography;
    }

    /**
     * @param mixed $biography
     */
    public function setBiography($biography)
    {
        $this->biography = $biography;
    }

    /**
     * Add personMovie
     *
     * @param \BackendBundle\Entity\PersonMovie $personMovie
     *
     * @return Person
     */
    public function addPersonMovie(PersonMovie $personMovie)
    {
        $this->person_movie[] = $personMovie;

        return $this;
    }

    /**
     * Remove personMovie
     *
     * @param \BackendBundle\Entity\PersonMovie $personMovie
     */
    public function removePersonMovie(PersonMovie $personMovie)
    {
        $this->person_movie->removeElement($personMovie);
    }

    /**
     * Get personMovie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonMovie()
    {
        return $this->person_movie;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * @param mixed $full_name
     */
    public function setFullName($full_name)
    {
        $this->full_name = $full_name;
    }

    /**
     * @return mixed
     */
    public function getDeathDate()
    {
        return $this->death_date;
    }

    /**
     * @param mixed $death_date
     */
    public function setDeathDate($death_date)
    {
        $this->death_date = $death_date;
    }

    /**
     * @return mixed
     */
    public function getPopularity()
    {
        return $this->popularity;
    }

    /**
     * @param mixed $popularity
     */
    public function setPopularity($popularity)
    {
        $this->popularity = $popularity;
    }

    /**
     * @return mixed
     */
    public function getProfilePath()
    {
        return $this->profile_path;
    }

    /**
     * @param mixed $profile_path
     */
    public function setProfilePath($profile_path)
    {
        $this->profile_path = $profile_path;
    }


    /**
     * Add movieNomination
     *
     * @param \BackendBundle\Entity\MovieNomination $movieNomination
     *
     * @return Person
     */
    public function addMovieNomination(\BackendBundle\Entity\MovieNomination $movieNomination)
    {
        $this->movie_nomination[] = $movieNomination;

        return $this;
    }

    /**
     * Remove movieNomination
     *
     * @param \BackendBundle\Entity\MovieNomination $movieNomination
     */
    public function removeMovieNomination(\BackendBundle\Entity\MovieNomination $movieNomination)
    {
        $this->movie_nomination->removeElement($movieNomination);
    }

    /**
     * Get movieNomination
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovieNomination()
    {
        return $this->movie_nomination;
    }

    /**
     * Add personTvSeason
     *
     * @param \BackendBundle\Entity\PersonTvSeason $personTvSeason
     *
     * @return Person
     */
    public function addPersonTvSeason(\BackendBundle\Entity\PersonTvSeason $personTvSeason)
    {
        $this->person_tv_season[] = $personTvSeason;

        return $this;
    }

    /**
     * Remove personTvSeason
     *
     * @param \BackendBundle\Entity\PersonTvSeason $personTvSeason
     */
    public function removePersonTvSeason(\BackendBundle\Entity\PersonTvSeason $personTvSeason)
    {
        $this->person_tv_season->removeElement($personTvSeason);
    }

    /**
     * Get personTvSeason
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonTvSeason()
    {
        return $this->person_tv_season;
    }
}
