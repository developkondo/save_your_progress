<?php
/**
 * Created by Roman Bas.
 * User: romabas
 * Date: 11/06/18
 * Time: 15:49
 */

namespace BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="movie_nomination")
 */
class MovieNomination{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="BackendBundle\Entity\Person", inversedBy="movie_nomination")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity="BackendBundle\Entity\Award", inversedBy="movie_nomination")
     * @ORM\JoinColumn(name="award_id", referencedColumnName="id")
     */
    private $award;

    /**
     * @ORM\Column(type="integer", length=6)
     */
    private $movie;

    /**
     * @ORM\Column(type="integer")
     */
    private $winner = 0;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set movie
     *
     * @param integer $movie
     *
     * @return MovieNomination
     */
    public function setMovie($movie)
    {
        $this->movie = $movie;

        return $this;
    }

    /**
     * Get movie
     *
     * @return integer
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * Set winner
     *
     * @param integer $winner
     *
     * @return MovieNomination
     */
    public function setWinner($winner)
    {
        $this->winner = $winner;

        return $this;
    }

    /**
     * Get winner
     *
     * @return integer
     */
    public function getWinner()
    {
        return $this->winner;
    }

    /**
     * Set person
     *
     * @param \BackendBundle\Entity\Person $person
     *
     * @return MovieNomination
     */
    public function setPerson(\BackendBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \BackendBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set award
     *
     * @param \BackendBundle\Entity\Award $award
     *
     * @return MovieNomination
     */
    public function setAward(\BackendBundle\Entity\Award $award = null)
    {
        $this->award = $award;

        return $this;
    }

    /**
     * Get award
     *
     * @return \BackendBundle\Entity\Award
     */
    public function getAward()
    {
        return $this->award;
    }
}
