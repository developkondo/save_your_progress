<?php

namespace BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Intl\Intl;

use BackendBundle\Services\Tools;
use BackendBundle\Services\DoctrineHandler;

use BackendBundle\Entity\MovieNomination;
use BackendBundle\Entity\Collection;
use BackendBundle\Entity\Season;
use BackendBundle\Entity\Movie;
use BackendBundle\Entity\Country;
use BackendBundle\Entity\Genre;
use BackendBundle\Entity\Award;
use BackendBundle\Entity\Person;
use BackendBundle\Entity\PersonMovie;
use BackendBundle\Entity\Producer;

class DefaultController extends Controller
{
    public function indexAction()
    {

        return $this->render('@Backend/Default/index.html.twig');
    }

    public function FAGetLinksAction(Request $request){
        set_time_limit(0);
//        $url = "https://www.filmaffinity.com/es/film133653.html"; //Sin musica
//        $url = "https://www.filmaffinity.com/es/film652266.html"; //con premios
//        $url = "https://www.filmaffinity.com/es/film192757.html";
//        $url = "https://www.filmaffinity.com/es/film10.html";
        //$url = "https://www.filmaffinity.com/es/film101047.html"; //empty review


        $i_start = 445220;
        $i_end = $i_start + 30000;

        for ($i = $i_start; $i < $i_end; $i++) {
            $url = "https://www.filmaffinity.com/es/film" . $i . ".html";
            $headers = get_headers($url);

            if (strpos($headers[0], "200 OK")) {
                file_put_contents($this->get("kernel")->getRootDir() . "/filmaffinity_link_matches.txt", $url . PHP_EOL, FILE_APPEND);
            } else {
                echo "<pre>";
                print_r($url . " not found " . PHP_EOL);
            }
            $rand = rand(0, 1);
            sleep($rand);
        }

        die();
    }

    public function testAction(){
        die();
    }

    public function FANewRegistryAction($url = ""){
        $em = $this->getDoctrine()->getManager();

        $doctrine_handler = new DoctrineHandler($em);
        $tools = new Tools();

        $content = file_get_contents($url);
        $crawler = new Crawler($content);

        //FA id
        $id_fa_object = $crawler->filter('div[class="rate-movie-box"]');
        foreach ($id_fa_object as $id_fa){
            $id_fa = $id_fa->getAttribute('data-movie-id');
        }

        $isset_id_fa = $em->getRepository('BackendBundle:Movie')->findOneBy(array(
            "id_fa" => $id_fa
        ));

        //Check if the movie is already registred
        if(!empty($isset_id_fa)){
            $response = array(
                "code" => "ERROR",
                "msg" => "Film already registred."
            );
        } else {
            $start_time = time();
            $movie = new Movie();

            //Crawling

            if($crawler->filter('dl[class="movie-info"] dd span')->count() > 0){
                //Get original title and remove word "aka" from the end
                $original_title = substr(trim($crawler->filter('dl[class="movie-info"] dd')->text()), "0", "-3");
            } else{
                $original_title = trim($crawler->filter('dl[class="movie-info"] dd')->text());
            }

            $main_title = trim($crawler->filter('#main-title')->text());

            //Cleaning film name
            if(preg_match("/.*\(TV Series\)/",$original_title)){
                $original_title = trim(str_replace("(TV Series)","",$original_title));
                $main_title = trim(str_replace("(TV Series)","",$main_title));
            } elseif (preg_match("/.*\(S\)/",$original_title)){
                $original_title = trim(str_replace("(S)","",$original_title));
                $main_title = trim(str_replace("(S)","",$main_title));
            } elseif(preg_match("/.*\(TV Miniseries\)/",$original_title)){
                $original_title = trim(str_replace("(TV Miniseries)","",$original_title));
                $main_title = trim(str_replace("(TV Miniseries)","",$main_title));
            } elseif(preg_match("/.*\(TV\)/",$original_title)){
                $original_title = trim(str_replace("(TV)","",$original_title));
                $main_title = trim(str_replace("(TV)","",$main_title));
            }
            $date_published = $crawler->filter('dd[itemprop="datePublished"]')->text();
            $duration = str_replace("min.", "", $crawler->filter('dd[itemprop="duration"]')->text());

            $movie->setIdFa($id_fa);
            $movie->setOriginalTitle(trim($original_title));
            $movie->setMainTitle(trim($main_title));
            $movie->setPremiereDate(trim($date_published));
            $movie->setDuration(trim($duration));

            $country_name = trim($crawler->filter('dd span[id="country-img"] img')->attr('title'));

            $isset_country = $em->getRepository('BackendBundle:Country')->findOneBy(array(
                "country_name" => $country_name
            ));

            if($isset_country == null){
                $country = new Country();
                $country_code = $tools->getCountryCodeFromCountryName($country_name);
                $country->setCountryName($country_name);
                $country->setCountryCode($country_code);
                $em->persist($country);
            } else{
                $country = $isset_country;
            }

            $movie->setCountry($country);


            //Rating

            $rating_fa_avg = $crawler->filter('div[id="movie-rat-avg"]')->text();
            $rating_fa_count = $crawler->filter('div[id="movie-count-rat"] span[itemprop="ratingCount"]')->text();

            $movie->setRatingFaAvg(trim($rating_fa_avg));
            $movie->setRatingFaCount(trim(str_replace(",", "", $rating_fa_count)));

            //Synopsis
            $synopsis = $crawler->filter('dd[itemprop="description"]')->text();
            $synopsis = str_replace("(FILMAFFINITY)", "", $synopsis);

            $movie->setSynopsis(trim($synopsis));


            $em->persist($movie);
            $em->flush();

            //Check if the "person" is registered.
            //If registered -> check if he has a director occupation.
            //If is not a director -> register as a director.
            //If not registered -> register with director occupation.
            $directors = $crawler->filter('span[itemprop="director"] a ');

            foreach ($directors as $director) {
                $occupation = "director";
                $doctrine_handler->personProcedure($director->nodeValue, $movie, $occupation);
                $em->flush();
            }
            preg_match_all('/<dd><div class="credits"><span class="nb"><span>(.*)/', $content, $credits_match);
            $credits = $crawler->filter('div[id="left-column"] dl[class="movie-info"] dt');
            $save_credit_person = false;
            $match_element_number = 0;
            foreach ($credits as $credit) {
                $credit_title = $credit->nodeValue;

                //Getting the correct value of credit person
                switch ($credit_title) {
                    case "Screenwriter":
                        $film_screenwriter = $credits_match[1][$match_element_number];
                        $match_element_number++;
                        $film_credit_persons = explode(",", $film_screenwriter);
                        $occupation = "screenwriter";
                        if (count($film_credit_persons) == 1) {
                            $film_credit_persons[0] = $film_screenwriter;
                        }

                        $save_credit_person = true;
                        break;
                    case "Music":
                        $film_music = $credits_match[1][$match_element_number];
                        $match_element_number++;
                        $film_credit_persons = explode(",", $film_music);
                        $occupation = "music";
                        if (count($film_credit_persons) == 1) {
                            $film_credit_persons[0] = $film_music;
                        }

                        $save_credit_person = true;
                        break;
                    case "Cinematography":
                        $film_cinematography = $credits_match[1][$match_element_number];
                        $match_element_number++;
                        $film_credit_persons = explode(",", $film_cinematography);
                        $occupation = "cinematography";
                        if (count($film_credit_persons) == 1) {
                            $film_credit_persons[0] = $film_cinematography;
                        }

                        $save_credit_person = true;
                        break;
                    case "Producer":
                        //TODO -> check for "broadcasting"
                        $film_producer = $credits_match[1][$match_element_number];
                        $match_element_number++;
                        //Looking for ";"
                        $film_producer = str_replace(";", "/", $film_producer);

                        $film_credit_producers = explode("/", strip_tags($film_producer));

                        if (count($film_credit_producers) == 1) {
                            $film_credit_producers[0] = $film_producer;
                        }

                        foreach ($film_credit_producers as $film_credit_producer){
                            $film_credit_producer = trim($tools->cleanStringBetweenTags($film_credit_producer,"(", ")", true));
                            $isset_producer = $em->getRepository('BackendBundle:Producer')->findOneBy(array(
                                "producer_name" => $film_credit_producer
                            ));

                            if(empty($isset_producer)){
                                $producer = new Producer();
                                $producer->setProducerName($film_credit_producer);
                                $em->persist($producer);
                            } else{
                                $producer = $isset_producer;
                            }

                            $movie->addProducer($producer);

                        }

                        break;
                }
                if($save_credit_person){
                    //Inserting values of a credit person
                    foreach ($film_credit_persons as $film_credit_person) {
                        $doctrine_handler->personProcedure(trim(strip_tags($film_credit_person)),$movie,$occupation);
                        $em->flush();
                    }
                }
                $save_credit_person = false;
            }

            $actors = $crawler->filter('span[itemprop="actor"] a span');
            $character_popularity = 1;
            foreach ($actors as $actor) {
                $actor = trim($actor->nodeValue);
                $occupation = "actor";
                $doctrine_handler->personProcedure($actor,$movie,$occupation,$character_popularity);
                $em->flush();
                $character_popularity++;
            }

            //Getting the html with the "Genres" content
            preg_match_all("/<dt>Genre<\/dt>(.*?)<\/dd>/s", $content, $genres_match);

            //Instantiate new Crawler object to get the content of genres.
            $crawler_genre = new Crawler($genres_match[1][0]);
            $genres = $crawler_genre->filter('dd a');
            foreach ($genres as $film_genre) {
                $film_genre = trim($film_genre->nodeValue);
                $isset_genre = $em->getRepository('BackendBundle:Genre')->findOneBy(array(
                    "genre_name" => $film_genre
                ));

                if($isset_genre == null){
                    $genre = new Genre();
                    $genre->setGenereName($film_genre);
                    $em->persist($genre);
                } else{
                    $genre = $isset_genre;
                }

                $movie->addGenre($genre);
            }

            $groups = $crawler->filter('dd[style="position: relative;"] span');
            foreach ($groups as $film_group) {
                $film_group = trim($film_group->nodeValue);
                $isset_group = $em->getRepository('Collection.php')->findOneBy(array(
                    "group_name" => $film_group
                ));

                if($isset_group == null){
                    $group = new Collection();
                    $group->setGroupName($film_group);
                    $em->persist($film_group);
                } else{
                    $group = $isset_group;
                }

                $movie->addGroup($group);
            }

            $em->persist($movie);
            $em->flush();

            $end_time = time();

            $response = array(
                "status" => "SUCCESS",
                "msg" => "Film registered",
                "time" => $end_time - $start_time . " seconds"
            );

        }

        return $movie;
    }

    public function FANewAwardAction(){
        $em = $this->getDoctrine()->getManager();

        $doctrine_handler = new DoctrineHandler($em);
        $tools = new Tools();

        $award_id = "";
        $year = "";
        $url = "https://www.filmaffinity.com/en/awards.php?award_id=$award_id&year=$year";
        $url = "https://www.filmaffinity.com/en/awards.php?award_id=european&year=2017";

        $content = file_get_contents($url);
        $crawler = new Crawler($content);

        $award_title = $crawler->filter('h1[id="main-title"]')->text();

        //TODO -> get number between parenthesis
        $award_year = "";
        $nominations = $crawler->filterXPath('//div[@class="awards-left-container"]');

        foreach ($nominations as $nomination) {
            $nomination_crawler = new Crawler($nomination);
            $nomination_categories = $nomination_crawler->filter('div[class="full-content"]');
            foreach ($nomination_categories as $nomination_category) {

                $nomination_category_crawler = new Crawler($nomination_category);
                $category_name = $nomination_category_crawler->filter('div[class="header"]')->text();

                $isset_category_award = $em->getRepository('BackendBundle:Award')->findOneBy(array(
                    "category" => $category_name,
                    "name" => $award_title,
                    "year" => $award_year
                ));

                if(empty($isset_category_award)){
                    $award = $isset_category_award;
                } else{
                    $award = new Award();
                    $award->setName($award_title);
                    $award->setCategory($category_name);
                    $award->setYear($award_year);

                    $em->persist($award);
                }

                $category_winner_person = $nomination_category_crawler->filter('div[class="wrapper"] ul li[class="fa-shadow win-row"] div div');
                $category_winner_film = $nomination_category_crawler->filter('div[class="wrapper"] ul li[class="fa-shadow win-row"] div[class="aw-mc"] a');

                preg_match_all("/.*?film(.*?).html/", $category_winner_film->attr("href"), $movie_id_match);
                $movie_winner_id = $movie_id_match[1][0];
                $isset_movie = $em->getRepository('BackendBundle:Movie')->findOneBy(array(
                    "id_fa" => $movie_winner_id
                ));

                if(empty($isset_movie)){
                    $movie_winner = $this->FANewRegistryAction("https://www.filmaffinity.com/en/film" . $movie_winner_id . ".html");
                } else{
                    $movie_winner = $isset_movie;
                }

                if ($category_winner_person->attr('class') == "nom-text") {
                    //PERSON WINNER
                    //TODO CHECK IF FIND PERSON MOVIE IS NECESARY
                    $person_movie = $doctrine_handler->findPersonMovie($movie_winner->getId(),$category_winner_person->text());
                    $movie_nomination = new MovieNomination();
                    $movie_nomination->setAward($award);
                    $movie_nomination->setPersonMovie($person_movie);
                    //TODO CREATE MOVIE relationship
                    $movie_nomination->setMovie($movie_winner->getId());
                    $movie_nomination->setWinner(1);
                } else {
                    //FILM WINNER
                    $movie_nomination = new MovieNomination();
                    $movie_nomination->setAward($award);
                    $movie_nomination->setPersonMovie(null);
                    $movie_nomination->setMovie($movie_winner->getId());
                    $movie_nomination->setWinner(1);
                }
                //Persist the winner
                $em->persist($movie_nomination);

                $category_nominated_entity_html = $nomination_category_crawler->filter('div[class="wrapper"]')->html();

                $category_nominated_entity_crawler = new Crawler($category_nominated_entity_html);

                $category_nominated_entities = $category_nominated_entity_crawler->filter('ul[class="nms"] li[class="fa-shadow"]');

                foreach ($category_nominated_entities as $category_nominated_entity){
                    $category_nominated_entity_element_crawler = new Crawler($category_nominated_entity);

                    $category_nominated_person = $category_nominated_entity_element_crawler->filter('div[class="nom-text"]');
                    $category_nominated_film = $category_nominated_entity_element_crawler->filter('div[class="aw-mc"] a[class="movie-title-link"]');

                    preg_match_all("/.*?film(.*?).html/", $category_nominated_film->attr("href"), $movie_id_match);
                    $movie_winner_id = $movie_id_match[1][0];
                    $isset_movie = $em->getRepository('BackendBundle:Movie')->findOneBy(array(
                        "id_fa" => $movie_winner_id
                    ));

                    if(empty($isset_movie)){
                        $movie_nominated = $this->FANewRegistryAction("https://www.filmaffinity.com/en/film" . $movie_winner_id . ".html");
                    } else{
                        $movie_nominated = $isset_movie;
                    }

                    if($category_nominated_person->count() > 0){
                        //PERSON NOMINATION
                        $person_movie = $doctrine_handler->findPersonMovie($movie_nominated->getId(),$category_nominated_person->text());

                        $movie_nomination = new MovieNomination();
                        $movie_nomination->setAward($award);
                        $movie_nomination->setPersonMovie($person_movie);
                        $movie_nomination->setMovie($movie_nominated->getId());
                    } else{
                        //FILM NOMINATION
                        $movie_nomination = new MovieNomination();
                        $movie_nomination->setAward($award);
                        $movie_nomination->setPersonMovie(null);
                        $movie_nomination->setMovie($movie_nominated->getId());
                    }
                    $em->persist($movie_nomination);
                }

            }
        }
        $em->flush();
        die();
    }

}
