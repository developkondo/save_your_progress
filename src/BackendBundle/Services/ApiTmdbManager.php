<?php
/**
 * Created by Roman Bas.
 * User: romabas
 * Date: 8/08/18
 * Time: 14:04
 */

namespace BackendBundle\Services;


class ApiTmdbManager{

    const api_key = "";
    const image_base_url = "http://image.tmdb.org/t/p/"; //EXAMPLE = http://image.tmdb.org/t/p/w185/MZeLxOH0PgL7xcvt865WVBvQDw.jpg

    const award_repository = "BackendBundle:Award";
    const collection_repository = "BackendBundle:Collection";
    const country_repository = "BackendBundle:Country";
    const genre_repository = "BackendBundle:Genre";
    const movie_repository = "BackendBundle:Movie";
    const movie_nomination_repository = "BackendBundle:MovieNomination";
    const person_repository = "BackendBundle:Person";
    const person_movie_repository = "BackendBundle:PersonMovie";
    const producer_repository = "BackendBundle:Producer";
    const season_repository = "BackendBundle:Season";

    public function getApiKey(){
        return self::api_key;
    }

    public function getImageBaseUrl(){
        return self::image_base_url;
    }

    public function getAwardRepository(){
        return self::award_repository;
    }

    public function getCollectionRepository(){
        return self::collection_repository;
    }

    public function getCountryRepository(){
        return self::country_repository;
    }

    public function getGenreRepository(){
        return self::genre_repository;
    }

    public function getMovieRepository(){
        return self::movie_repository;
    }

    public function getMovieNominationRepository(){
        return self::movie_nomination_repository;
    }

    public function getPersonRepository(){
        return self::person_repository;
    }

    public function getPersonMovieRepository(){
        return self::person_movie_repository;
    }

    public function getProducerRepository(){
        return self::producer_repository;
    }

    public function getSeasonRepository(){
        return self::season_repository;
    }
}