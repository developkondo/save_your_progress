<?php
/**
 * Created by Roman Bas.
 * User: romabas
 * Date: 25/07/18
 * Time: 14:15
 */

namespace BackendBundle\Controller;

use BackendBundle\Services\ApiTmdbManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use BackendBundle\Entity\MovieNomination;
use BackendBundle\Entity\Collection;
use BackendBundle\Entity\Season;
use BackendBundle\Entity\Movie;
use BackendBundle\Entity\Country;
use BackendBundle\Entity\Genre;
use BackendBundle\Entity\Award;
use BackendBundle\Entity\Person;
use BackendBundle\Entity\PersonMovie;
use BackendBundle\Entity\Producer;

use BackendBundle\Services\Tools;

class ApiTmdbController extends Controller{
    private $amount_api_calls = 1;
    private $api_interval = 0;

    public function __construct(){
        $this->tmdb_manager = new ApiTmdbManager();
    }

    public function testApiAction(){
        $api_key = $this->tmdb_manager->getApiKey();
        $tools = new Tools();

//        $this->generateMovies();
//        echo("<pre>");
//        print_r("ok");
//        die();
//        $url = "https://api.themoviedb.org/3/movie/1726?api_key=$api_key&language=en-US"; //MOVIE
//        $url = "https://api.themoviedb.org/3/movie/1726/credits?api_key=$api_key"; //CREDITS
//        $url = "https://api.themoviedb.org/3/person/3223?api_key=$api_key&language=en-US"; // PERSON
        $url = "https://api.themoviedb.org/3/tv/60059?api_key=3ba5451c7b493bf97a57200915a7e200&language=en-US"; //SERIE

//        for ($i = 0; $i <=45; $i++){
//            $data = $tools->make_call($url);
//            echo "<pre>";
//            print_r($data);
//
//            echo "<pre>";
//            print_r($i);
//        }
//        die();

        $data = $tools->make_call($url);

        $data_decoded = json_decode($data,true);

        echo "<pre>";
        print_r(($data_decoded));
        die();
    }

    public function generateMovies($movie_id = "", $language = "en-US"){
        set_time_limit(0);

        $em = $this->getDoctrine()->getManager();
        //Get last inserted movie
        $last_movie = $em->getRepository($this->tmdb_manager->getMovieRepository())->findOneBy(
            array(),
            array("id" => "DESC")
        );

        if($last_movie == null){
            $movie_id = 1;
        } else{
            $movie_id = $last_movie->getIdTmdb() + 1;
        }
        for($i = 0; $i<= 100; $i++){
            $movie_json = $this->getApiMovie($movie_id);

            $movie_data = json_decode($movie_json,true);

            if(isset($movie_data["status_code"]) && $movie_data["status_code"] == 34){
                $movie_id++;
            } else{
                $this->newMovie($movie_data);
                $movie_id++;

            }
        }
    }

    public function getApiMovie($movie_id = "", $language = "en-US"){
        $api_key = $this->tmdb_manager->getApiKey();
        $tools = new Tools();

        $this->checkApiCalls();
        $url = "https://api.themoviedb.org/3/movie/$movie_id?api_key=$api_key&language=$language";
        $this->setAmountApiCalls();

        $json = $tools->make_call($url);

        return $json;
    }

    public function newMovie($movie_data){
        $em = $this->getDoctrine()->getManager();
        $movie = new Movie();

        //MOVIE
        $movie->setIdImbd($movie_data["imdb_id"]);
        $movie->setIdTmdb($movie_data["id"]);
        $movie->setOriginalTitle($movie_data["original_title"]);
        $movie->setMainTitle($movie_data["title"]);
        $movie->setPosterPath($movie_data["poster_path"]);
        $movie->setBackdropPath($movie_data["backdrop_path"]);
        $movie->setDuration($movie_data["runtime"]);
        $movie->setPremiereDate(\DateTime::createFromFormat('Y-m-d', $movie_data["release_date"]));
        $movie->setBudget($movie_data["budget"]);
        $movie->setRevenue($movie_data["revenue"]);
        $movie->setRatingTmdbAvg($movie_data["vote_average"]);
        $movie->setRatingTmdbCount($movie_data["vote_count"]);
        $movie->setStatus($movie_data["status"]);
        $movie->setSynopsis($movie_data["overview"]);
        $movie->setTagline($movie_data["tagline"]);
        $movie->setPopularity($movie_data["popularity"]);
        $em->persist($movie);

        //COLLECTION
        if(isset($movie_data["belongs_to_collection"])){
            $collection_exists = $this->getEntityIfExists($em, $this->tmdb_manager->getCollectionRepository(),"name", $movie_data["belongs_to_collection"]["name"]);
            if($collection_exists){
                $collection = $collection_exists;
            } else{
                $collection = $this->newCollection($movie_data["belongs_to_collection"]);
                $em->persist($collection);
            }
            $movie->addCollection($collection);
        }

        //GENRES
        if(isset($movie_data["genres"])){
            foreach ($movie_data["genres"] as $genre_data){
                $genre_exists = $this->getEntityIfExists($em, $this->tmdb_manager->getGenreRepository(), "id", $genre_data["id"]);
                if($genre_exists){
                    $genre = $genre_exists;
                } else{
                    $genre = $this->newGenre($genre_data);
                    $em->persist($genre);
                }
                $movie->addGenre($genre);
            }
        }

        //PRODUCTION COMPANIES
        if(isset($movie_data["production_companies"])){
            foreach ($movie_data["production_companies"] as $production_company_data){
                $production_company_exists = $this->getEntityIfExists($em, $this->tmdb_manager->getProducerRepository(),"producer_name", $production_company_data["name"]);
                if($production_company_exists){
                    $producer = $production_company_exists;
                } else{
                    $producer = $this->newProducer($production_company_data);
                    $em->persist($producer);
                }
                $movie->addProducer($producer);
            }
        }

        //PRODUCTION COUNTRIES
        if(isset($movie_data["production_countries"])){
            foreach ($movie_data["production_countries"] as $production_country){
                $production_country_exists = $this->getEntityIfExists($em, $this->tmdb_manager->getCountryRepository(), "country_code",$production_country["iso_3166_1"]);
                if($production_country_exists){
                    $country = $production_country_exists;
                } else{
                    $country = $this->newCountry($production_country);
                    $em->persist($country);
                }
                $movie->addCountry($country);
            }
        }

        //CREDITS
        $this->newMovieCredits($movie_data["id"], "movie");

        $em->flush();

    }

    public function newTvSerie($tv_serie_data){

    }

    public function getApiCredits($tmdb_id,$type = "movie"){
        $api_key = $this->tmdb_manager->getApiKey();
        $tools = new Tools();

        $this->checkApiCalls();
        $url = "https://api.themoviedb.org/3/$type/$tmdb_id/credits?api_key=$api_key";
        $this->setAmountApiCalls();

        $credits_json_data = $tools->make_call($url);

        return $credits_json_data;
    }

    public function newMovieCredits($tmbd_movie_id, $type){
        $em = $this->getDoctrine()->getManager();
        $credits_json = $this->getApiCredits($tmbd_movie_id,$type);
        $movie = $this->getEntityIfExists($em,$this->tmdb_manager->getMovieRepository(), "id", $tmbd_movie_id);

        $credits_data = json_decode($credits_json,true);

        //ACTORS
        $cast = $credits_data["cast"];

        foreach ($cast as $actor){
            $person_exists = $this->getEntityIfExists($em,$this->tmdb_manager->getPersonRepository(), "id", $actor["id"]);
            if($person_exists){
                $person = $person_exists; //Person exists
            } else{
                $person_data = $this->getApiPerson($actor["id"]);

                $person = $this->newPerson($person_data);

                $em->persist($person);
            }
            $person_movie = new PersonMovie();
            $person_movie->setMovie($movie);
            $person_movie->setPerson($person);
            $person_movie->setJob("Cast");
            $person_movie->setCharacter($actor["character"]);
            $person_movie->setOrder($actor["order"]);
            $em->persist($person_movie);
        }

        //CREW
        $crew = $credits_data["crew"];
        foreach ($crew as $crew_member){
            $person_exists = $this->getEntityIfExists($em,$this->tmdb_manager->getPersonRepository(), "id", $crew_member["id"]);
            if($person_exists){
                $person = $person_exists; //Person exists
            } else{
                $person_data = $this->getApiPerson($crew_member["id"]);
                $person = $this->newPerson($person_data);
                $em->persist($person);
            }
            $person_movie = new PersonMovie();
            $person_movie->setMovie($movie);
            $person_movie->setPerson($person);
            $person_movie->setJob($crew_member["job"]);
            $em->persist($person_movie);
        }

        $em->flush();
    }

    public function getEntityIfExists($em, $repository, $field, $value){
        $isset_entity = $em->getRepository($repository)->findOneBy(
            array(
                $field => $value
            )
        );

        if($isset_entity == null){
            return false;
        } else{
            return $isset_entity;
        }
    }

    public function newCollection($collection_data){
        $collection = new Collection();
        $collection->setId($collection_data["id"]);
        $collection->setName($collection_data["name"]);
        $collection->setBackdropPath($collection_data["backdrop_path"]);
        $collection->setPosterPath($collection_data["poster_path"]);

        return $collection;
    }

    public function newGenre($genre_data){
        $genre = new Genre();
        $genre->setId($genre_data["id"]);
        $genre->setGenreName($genre_data["name"]);

        return $genre;
    }

    public function newProducer($producer_data){
        $producer = new Producer();
        $producer->setProducerName($producer_data["name"]);
        $producer->setLogoPath($producer_data["logo_path"]);

        return $producer;
    }

    public function newPerson($person_data){
        $person = new Person();
        $person->setId($person_data["id"]);
        $person->setPopularity($person_data["popularity"]);
        $person->setBiography($person_data["biography"]);
        $person->setBirthDate($person_data["birthday"]);
        $person->setBirthPlace($person_data["place_of_birth"]);
        $person->setDeathDate($person_data["deathday"]);
        $person->setFullName($person_data["name"]);
        $person->setGender($person_data["gender"]);
        $person->setProfilePath($person_data["profile_path"]);

        return $person;
    }

    public function newCountry($country_data){
        $country = new Country();
        $country->setCountryCode($country_data["iso_3166_1"]);
        $country->setCountryName($country_data["name"]);

        return $country;
    }

    public function getApiPerson($person_id, $language = "en-US"){
        $api_key = $this->tmdb_manager->getApiKey();
        $tools = new Tools();

        $this->checkApiCalls();
        $url = "https://api.themoviedb.org/3/person/$person_id?api_key=$api_key&language=$language";
        $this->setAmountApiCalls();

        $json = $tools->make_call($url);

        return $json;
    }

    public function setAmountApiCalls(){
        return $this->amount_api_calls++;
    }

    public function checkApiCalls(){
        if($this->amount_api_calls == 40){
            sleep(10);
        }
    }
}