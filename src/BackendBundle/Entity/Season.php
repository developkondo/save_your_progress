<?php
/**
 * Created by Roman Bas.
 * User: roman
 * Date: 08/06/2018
 * Time: 18:59
 */

namespace BackendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="season")
 */
class Season{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $season_release;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount_episodes;

    /**
     * @ORM\Column(type="integer")
     */
    private $season_number;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $name = null;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $poster_path = null;

    /**
     * @ORM\Column(type="text")
     */
    private $overview = "";

    /**
     * @ORM\OneToMany(targetEntity="BackendBundle\Entity\PersonTvSeason", mappedBy="tv_season", cascade={"persist"})
     */
    private $person_tv_season;

    /**
     * @ORM\ManyToOne(targetEntity="BackendBundle\Entity\TvSerie", inversedBy="seasons")
     * @ORM\JoinColumn(name="tv_serie_id", referencedColumnName="id")
     */
    private $tv_series;

    public function __construct(){
        $this->person_tv_season = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set seasonYear
     *
     * @param integer $seasonYear
     *
     * @return Season
     */
    public function setSeasonYear($seasonYear)
    {
        $this->season_year = $seasonYear;

        return $this;
    }

    /**
     * Get seasonYear
     *
     * @return integer
     */
    public function getSeasonYear()
    {
        return $this->season_year;
    }

    /**
     * Set amountEpisodes
     *
     * @param integer $amountEpisodes
     *
     * @return Season
     */
    public function setAmountEpisodes($amountEpisodes)
    {
        $this->amount_episodes = $amountEpisodes;

        return $this;
    }

    /**
     * Get amountEpisodes
     *
     * @return integer
     */
    public function getAmountEpisodes()
    {
        return $this->amount_episodes;
    }

    /**
     * Set seasonNumber
     *
     * @param integer $seasonNumber
     *
     * @return Season
     */
    public function setSeasonNumber($seasonNumber)
    {
        $this->season_number = $seasonNumber;

        return $this;
    }

    /**
     * Get seasonNumber
     *
     * @return integer
     */
    public function getSeasonNumber()
    {
        return $this->season_number;
    }

    /**
     * Set movies
     *
     * @param \BackendBundle\Entity\Movie $movies
     *
     * @return Season
     */
    public function setMovies(\BackendBundle\Entity\Movie $movies = null)
    {
        $this->movies = $movies;

        return $this;
    }

    /**
     * Get movies
     *
     * @return \BackendBundle\Entity\Movie
     */
    public function getMovies()
    {
        return $this->movies;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Season
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set seasonRelease
     *
     * @param \DateTime $seasonRelease
     *
     * @return Season
     */
    public function setSeasonRelease($seasonRelease)
    {
        $this->season_release = $seasonRelease;

        return $this;
    }

    /**
     * Get seasonRelease
     *
     * @return \DateTime
     */
    public function getSeasonRelease()
    {
        return $this->season_release;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Season
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set posterPath
     *
     * @param string $posterPath
     *
     * @return Season
     */
    public function setPosterPath($posterPath)
    {
        $this->poster_path = $posterPath;

        return $this;
    }

    /**
     * Get posterPath
     *
     * @return string
     */
    public function getPosterPath()
    {
        return $this->poster_path;
    }

    /**
     * Set overview
     *
     * @param string $overview
     *
     * @return Season
     */
    public function setOverview($overview)
    {
        $this->overview = $overview;

        return $this;
    }

    /**
     * Get overview
     *
     * @return string
     */
    public function getOverview()
    {
        return $this->overview;
    }

    /**
     * Add personTvSeason
     *
     * @param \BackendBundle\Entity\PersonTvSeason $personTvSeason
     *
     * @return Season
     */
    public function addPersonTvSeason(\BackendBundle\Entity\PersonTvSeason $personTvSeason)
    {
        $this->person_tv_season[] = $personTvSeason;

        return $this;
    }

    /**
     * Remove personTvSeason
     *
     * @param \BackendBundle\Entity\PersonTvSeason $personTvSeason
     */
    public function removePersonTvSeason(\BackendBundle\Entity\PersonTvSeason $personTvSeason)
    {
        $this->person_tv_season->removeElement($personTvSeason);
    }

    /**
     * Get personTvSeason
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonTvSeason()
    {
        return $this->person_tv_season;
    }

    /**
     * Set tvSeries
     *
     * @param \BackendBundle\Entity\TvSerie $tvSeries
     *
     * @return Season
     */
    public function setTvSeries(\BackendBundle\Entity\TvSerie $tvSeries = null)
    {
        $this->tv_series = $tvSeries;

        return $this;
    }

    /**
     * Get tvSeries
     *
     * @return \BackendBundle\Entity\TvSerie
     */
    public function getTvSeries()
    {
        return $this->tv_series;
    }
}
