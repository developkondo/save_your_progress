<?php
/**
 * Created by Roman Bas.
 * User: romabas
 * Date: 13/08/18
 * Time: 12:53
 */

namespace BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="person_tv_season")
 */
class PersonTvSeason{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=35)
     */
    private $job;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $character = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $order = null;

    /**
     * @ORM\ManyToOne(targetEntity="BackendBundle\Entity\Person", inversedBy="person_tv_season")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     */
    private $person;

    /**
     * @ORM\ManyToOne(targetEntity="BackendBundle\Entity\Season", inversedBy="person_tv_season")
     * @ORM\JoinColumn(name="season_id", referencedColumnName="id")
     */
    private $tv_season;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set job
     *
     * @param string $job
     *
     * @return PersonTvSeason
     */
    public function setJob($job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return string
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set character
     *
     * @param string $character
     *
     * @return PersonTvSeason
     */
    public function setCharacter($character)
    {
        $this->character = $character;

        return $this;
    }

    /**
     * Get character
     *
     * @return string
     */
    public function getCharacter()
    {
        return $this->character;
    }

    /**
     * Set order
     *
     * @param integer $order
     *
     * @return PersonTvSeason
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set person
     *
     * @param \BackendBundle\Entity\Person $person
     *
     * @return PersonTvSeason
     */
    public function setPerson(\BackendBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \BackendBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set tvSeason
     *
     * @param \BackendBundle\Entity\Season $tvSeason
     *
     * @return PersonTvSeason
     */
    public function setTvSeason(\BackendBundle\Entity\Season $tvSeason = null)
    {
        $this->tv_season = $tvSeason;

        return $this;
    }

    /**
     * Get tvSeason
     *
     * @return \BackendBundle\Entity\Season
     */
    public function getTvSeason()
    {
        return $this->tv_season;
    }
}
