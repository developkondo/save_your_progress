<?php
/**
 * Created by Roman Bas.
 * User: roman
 * Date: 08/06/2018
 * Time: 18:58
 */

namespace BackendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="movie")
 */
class Movie{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $original_title;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $main_title;

    /**
     * @ORM\Column(type="date")
     */
    private $premiere_date;

    /**
     * @ORM\Column(type="integer")
     */
    private $duration;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $budget = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $revenue = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $rating_fa_avg = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rating_fa_count = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $rating_tmdb_avg = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rating_tmdb_count = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $rating_imdb_avg = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $rating_syp_avg = null;

    /**
     * @ORM\Column(type="text")
     */
    private $synopsis;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_fa = 0;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $id_imbd = 0;

    /**
     * @ORM\Column(type="integer", unique=true)
     */
    private $id_tmdb = null;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $backdrop_path = null;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $poster_path = null;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $status = null;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $tagline = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $popularity = null;

    /**
     * @ORM\ManyToMany(targetEntity="BackendBundle\Entity\Country")
     * @ORM\JoinColumn(name="movie_country")
     */
    private $countries;

    /**
     * @ORM\ManyToMany(targetEntity="BackendBundle\Entity\Genre")
     * @ORM\JoinTable(name="movie_genre")
     */
    private $genres;

    /**
     * @ORM\ManyToMany(targetEntity="BackendBundle\Entity\Collection")
     * @ORM\JoinTable(name="movie_collection")
     */
    private $collection;

    /**
     * @ORM\ManyToMany(targetEntity="BackendBundle\Entity\Producer")
     * @ORM\JoinTable(name="movie_producer")
     */
    private $producers;

    /**
     * @ORM\OneToMany(targetEntity="BackendBundle\Entity\PersonMovie", mappedBy="movie", cascade={"persist"})
     */
    private $person_movie;

    public function __construct(){
        $this->genres = new ArrayCollection();
        $this->countries = new ArrayCollection();
        $this->collection = new ArrayCollection();
        $this->person_movie = new ArrayCollection();
        $this->producers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set originalTitle
     *
     * @param string $originalTitle
     *
     * @return Movie
     */
    public function setOriginalTitle($originalTitle)
    {
        $this->original_title = $originalTitle;

        return $this;
    }

    /**
     * Get originalTitle
     *
     * @return string
     */
    public function getOriginalTitle()
    {
        return $this->original_title;
    }

    /**
     * Set mainTitle
     *
     * @param string $mainTitle
     *
     * @return Movie
     */
    public function setMainTitle($mainTitle)
    {
        $this->main_title = $mainTitle;

        return $this;
    }

    /**
     * Get mainTitle
     *
     * @return string
     */
    public function getMainTitle()
    {
        return $this->main_title;
    }

    /**
     * Set premiereDate
     *
     * @param integer $premiereDate
     *
     * @return Movie
     */
    public function setPremiereDate($premiereDate)
    {
        $this->premiere_date = $premiereDate;

        return $this;
    }

    /**
     * Get premiereDate
     *
     * @return integer
     */
    public function getPremiereDate()
    {
        return $this->premiere_date;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return Movie
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set budget
     *
     * @param integer $budget
     *
     * @return Movie
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return integer
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set revenue
     *
     * @param integer $revenue
     *
     * @return Movie
     */
    public function setRevenue($revenue)
    {
        $this->revenue = $revenue;

        return $this;
    }

    /**
     * Get revenue
     *
     * @return integer
     */
    public function getRevenue()
    {
        return $this->revenue;
    }

    /**
     * Set ratingFaAvg
     *
     * @param float $ratingFaAvg
     *
     * @return Movie
     */
    public function setRatingFaAvg($ratingFaAvg)
    {
        $this->rating_fa_avg = $ratingFaAvg;

        return $this;
    }

    /**
     * Get ratingFaAvg
     *
     * @return float
     */
    public function getRatingFaAvg()
    {
        return $this->rating_fa_avg;
    }

    /**
     * Set ratingFaCount
     *
     * @param integer $ratingFaCount
     *
     * @return Movie
     */
    public function setRatingFaCount($ratingFaCount)
    {
        $this->rating_fa_count = $ratingFaCount;

        return $this;
    }

    /**
     * Get ratingFaCount
     *
     * @return integer
     */
    public function getRatingFaCount()
    {
        return $this->rating_fa_count;
    }

    /**
     * Set ratingTmdbAvg
     *
     * @param float $ratingTmdbAvg
     *
     * @return Movie
     */
    public function setRatingTmdbAvg($ratingTmdbAvg)
    {
        $this->rating_tmdb_avg = $ratingTmdbAvg;

        return $this;
    }

    /**
     * Get ratingTmdbAvg
     *
     * @return float
     */
    public function getRatingTmdbAvg()
    {
        return $this->rating_tmdb_avg;
    }

    /**
     * Set ratingTmdbCount
     *
     * @param integer $ratingTmdbCount
     *
     * @return Movie
     */
    public function setRatingTmdbCount($ratingTmdbCount)
    {
        $this->rating_tmdb_count = $ratingTmdbCount;

        return $this;
    }

    /**
     * Get ratingTmdbCount
     *
     * @return integer
     */
    public function getRatingTmdbCount()
    {
        return $this->rating_tmdb_count;
    }

    /**
     * Set ratingImdbAvg
     *
     * @param float $ratingImdbAvg
     *
     * @return Movie
     */
    public function setRatingImdbAvg($ratingImdbAvg)
    {
        $this->rating_imdb_avg = $ratingImdbAvg;

        return $this;
    }

    /**
     * Get ratingImdbAvg
     *
     * @return float
     */
    public function getRatingImdbAvg()
    {
        return $this->rating_imdb_avg;
    }

    /**
     * Set ratingSypAvg
     *
     * @param float $ratingSypAvg
     *
     * @return Movie
     */
    public function setRatingSypAvg($ratingSypAvg)
    {
        $this->rating_syp_avg = $ratingSypAvg;

        return $this;
    }

    /**
     * Get ratingSypAvg
     *
     * @return float
     */
    public function getRatingSypAvg()
    {
        return $this->rating_syp_avg;
    }

    /**
     * Set synopsis
     *
     * @param string $synopsis
     *
     * @return Movie
     */
    public function setSynopsis($synopsis)
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    /**
     * Get synopsis
     *
     * @return string
     */
    public function getSynopsis()
    {
        return $this->synopsis;
    }

    /**
     * Set idFa
     *
     * @param integer $idFa
     *
     * @return Movie
     */
    public function setIdFa($idFa)
    {
        $this->id_fa = $idFa;

        return $this;
    }

    /**
     * Get idFa
     *
     * @return integer
     */
    public function getIdFa()
    {
        return $this->id_fa;
    }

    /**
     * Set idImbd
     *
     * @param string $idImbd
     *
     * @return Movie
     */
    public function setIdImbd($idImbd)
    {
        $this->id_imbd = $idImbd;

        return $this;
    }

    /**
     * Get idImbd
     *
     * @return string
     */
    public function getIdImbd()
    {
        return $this->id_imbd;
    }

    /**
     * Set idTmdb
     *
     * @param integer $idTmdb
     *
     * @return Movie
     */
    public function setIdTmdb($idTmdb)
    {
        $this->id_tmdb = $idTmdb;

        return $this;
    }

    /**
     * Get idTmdb
     *
     * @return integer
     */
    public function getIdTmdb()
    {
        return $this->id_tmdb;
    }

    /**
     * Set backdropPath
     *
     * @param string $backdropPath
     *
     * @return Movie
     */
    public function setBackdropPath($backdropPath)
    {
        $this->backdrop_path = $backdropPath;

        return $this;
    }

    /**
     * Get backdropPath
     *
     * @return string
     */
    public function getBackdropPath()
    {
        return $this->backdrop_path;
    }

    /**
     * Set posterPath
     *
     * @param string $posterPath
     *
     * @return Movie
     */
    public function setPosterPath($posterPath)
    {
        $this->poster_path = $posterPath;

        return $this;
    }

    /**
     * Get posterPath
     *
     * @return string
     */
    public function getPosterPath()
    {
        return $this->poster_path;
    }

    /**
     * Add genre
     *
     * @param \BackendBundle\Entity\Genre $genre
     *
     * @return Movie
     */
    public function addGenre(\BackendBundle\Entity\Genre $genre)
    {
        $this->genres[] = $genre;

        return $this;
    }

    /**
     * Remove genre
     *
     * @param \BackendBundle\Entity\Genre $genre
     */
    public function removeGenre(\BackendBundle\Entity\Genre $genre)
    {
        $this->genres->removeElement($genre);
    }

    /**
     * Get genres
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGenres()
    {
        return $this->genres;
    }

    /**
     * Add collection
     *
     * @param \BackendBundle\Entity\Collection $collection
     *
     * @return Movie
     */
    public function addCollection(\BackendBundle\Entity\Collection $collection)
    {
        $this->collection[] = $collection;

        return $this;
    }

    /**
     * Remove collection
     *
     * @param \BackendBundle\Entity\Collection $collection
     */
    public function removeCollection(\BackendBundle\Entity\Collection $collection)
    {
        $this->collection->removeElement($collection);
    }

    /**
     * Get collection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Add producer
     *
     * @param \BackendBundle\Entity\Producer $producer
     *
     * @return Movie
     */
    public function addProducer(\BackendBundle\Entity\Producer $producer)
    {
        $this->producers[] = $producer;

        return $this;
    }

    /**
     * Remove producer
     *
     * @param \BackendBundle\Entity\Producer $producer
     */
    public function removeProducer(\BackendBundle\Entity\Producer $producer)
    {
        $this->producers->removeElement($producer);
    }

    /**
     * Get producers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducers()
    {
        return $this->producers;
    }

    /**
     * Add personMovie
     *
     * @param \BackendBundle\Entity\PersonMovie $personMovie
     *
     * @return Movie
     */
    public function addPersonMovie(\BackendBundle\Entity\PersonMovie $personMovie)
    {
        $this->person_movie[] = $personMovie;

        return $this;
    }

    /**
     * Remove personMovie
     *
     * @param \BackendBundle\Entity\PersonMovie $personMovie
     */
    public function removePersonMovie(\BackendBundle\Entity\PersonMovie $personMovie)
    {
        $this->person_movie->removeElement($personMovie);
    }

    /**
     * Get personMovie
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonMovie()
    {
        return $this->person_movie;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getTagline()
    {
        return $this->tagline;
    }

    /**
     * @param mixed $tagline
     */
    public function setTagline($tagline)
    {
        $this->tagline = $tagline;
    }

    /**
     * @return mixed
     */
    public function getPopularity()
    {
        return $this->popularity;
    }

    /**
     * @param mixed $popularity
     */
    public function setPopularity($popularity)
    {
        $this->popularity = $popularity;
    }


    /**
     * Add country
     *
     * @param \BackendBundle\Entity\Country $country
     *
     * @return Movie
     */
    public function addCountry(\BackendBundle\Entity\Country $country)
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param \BackendBundle\Entity\Country $country
     */
    public function removeCountry(\BackendBundle\Entity\Country $country)
    {
        $this->countries->removeElement($country);
    }

    /**
     * Get countries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }
}
