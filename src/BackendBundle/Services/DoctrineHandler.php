<?php
/**
 * Created by Roman Bas.
 * User: romabas
 * Date: 20/06/18
 * Time: 12:40
 */

namespace BackendBundle\Services;

use Doctrine\ORM\EntityManagerInterface;

use BackendBundle\Entity\Collection;
use BackendBundle\Entity\Season;
use BackendBundle\Entity\Movie;
use BackendBundle\Entity\Country;
use BackendBundle\Entity\Genre;
use BackendBundle\Entity\Award;
use BackendBundle\Entity\Person;
use BackendBundle\Entity\PersonMovie;

use BackendBundle\Services\Tools;

class DoctrineHandler{

    private $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

    public function personProcedure($person, $movie_object, $occupation, $character_popularity = null){
        $tools = new Tools();
        $em = $this->em;

        $person = $tools->cleanStringBetweenTags($person,"(", ")", true);

        $film_person_array_name = $tools->explodePersonName($person);

        $isset_person = $this->findPersonByName($person);

        if($isset_person == null){
            $this->createPersonAndOccupation($movie_object, "", $occupation, $film_person_array_name, $character_popularity);
        } else{
            $isset_credit_person_movie = $em->getRepository('BackendBundle:PersonMovie')->findOneBy(array(
                "person" => $isset_person->getId(),
                "occupation" => $occupation
            ));

            if($isset_credit_person_movie == null){
                $this->createPersonAndOccupation($movie_object, $isset_person, $occupation, $character_popularity);
            }
        }

    }

    public function createPersonAndOccupation($movie_object, $person_object = "", $occupation, $name_array = "", $character_popularity = null){
        $em = $this->em;

        if($person_object == ""){
            $person = new Person();
            $person->setName($name_array["name"]);
            $person->setLastName($name_array["last_name"]);
            $em->persist($person);
        } else{
            $person = $person_object;
        }

        $person_movie = new PersonMovie();

        $person_movie->setMovie($movie_object);
        $person_movie->setPerson($person);
        $person_movie->setOccupation($occupation);
        $person_movie->setCharacterPopularity($character_popularity);

        $em->persist($person_movie);

//        $em->flush();
    }

    public function findPersonMovie($movie_id, $person_full_name){
        $em = $this->em;
        $person = $this->findPersonByName($person_full_name);
        $person_movie = $em->getRepository('BackendBundle:PersonMovie')->findOneBy(array(
            "person" => $person,
            "movie" => $movie_id
        ));

        return $person_movie;
    }

    public function findPersonByName($person_full_name){
        $em = $this->em;
        $tools = new Tools();
        $person_array_name = $tools->explodePersonName($person_full_name);
        $person = $em->getRepository('BackendBundle:Person')->findOneBy(array(
            "name" => trim($person_array_name["name"]),
            "last_name" => trim($person_array_name["last_name"])
        ));

        return $person;
    }

}