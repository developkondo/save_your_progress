**TMDB GENDER:**

0 - Not specified<br>
1 - Female<br>
2 - Male<br>


**SYMFONY COMMANDS**

GENERATE ADD, REMOVE, GET, SET

`doctrine:generate:entities BackendBundle/Entity`


CREATE DATABASE

`doctrine:database:create`

VALIDATE SCHEMA

`doctrine:schema:validate`

CREATE TABLES

`doctrine:schema:update --force`