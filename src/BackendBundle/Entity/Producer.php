<?php
/**
 * Created by Roman Bas.
 * User: roman
 * Date: 23/06/2018
 * Time: 22:23
 */

namespace BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="producer")
 */
class Producer{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $producer_name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $logo_path = null;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProducerName()
    {
        return $this->producer_name;
    }

    /**
     * @param mixed $producer_name
     */
    public function setProducerName($producer_name)
    {
        $this->producer_name = $producer_name;
    }

    /**
     * @return mixed
     */
    public function getLogoPath()
    {
        return $this->logo_path;
    }

    /**
     * @param mixed $logo_path
     */
    public function setLogoPath($logo_path)
    {
        $this->logo_path = $logo_path;
    }
}
