<?php
/**
 * Created by Roman Bas.
 * User: roman
 * Date: 08/06/2018
 * Time: 22:40
 */

namespace BackendBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="award")
 */
class Award{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $category;

    /**
     * @ORM\Column(type="integer", length=3)
     */
    private $number_edition;

    /**
     * @ORM\Column(type="integer", length=4)
     */
    private $year;

    /**
     * @ORM\OneToMany(targetEntity="BackendBundle\Entity\MovieNomination", mappedBy="award")
     */
    private $movie_nomination;

    public function __construct(){
        $this->movie_nomination = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Award
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return Award
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set numberEdition
     *
     * @param integer $numberEdition
     *
     * @return Award
     */
    public function setNumberEdition($numberEdition)
    {
        $this->number_edition = $numberEdition;

        return $this;
    }

    /**
     * Get numberEdition
     *
     * @return integer
     */
    public function getNumberEdition()
    {
        return $this->number_edition;
    }

    /**
     * Add movieNomination
     *
     * @param \BackendBundle\Entity\MovieNomination $movieNomination
     *
     * @return Award
     */
    public function addMovieNomination(\BackendBundle\Entity\MovieNomination $movieNomination)
    {
        $this->movie_nomination[] = $movieNomination;

        return $this;
    }

    /**
     * Remove movieNomination
     *
     * @param \BackendBundle\Entity\MovieNomination $movieNomination
     */
    public function removeMovieNomination(\BackendBundle\Entity\MovieNomination $movieNomination)
    {
        $this->movie_nomination->removeElement($movieNomination);
    }

    /**
     * Get movieNomination
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovieNomination()
    {
        return $this->movie_nomination;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }


}
