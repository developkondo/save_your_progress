<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 10/06/2018
 * Time: 21:18
 */

namespace BackendBundle\Services;

use Symfony\Component\Intl\Intl;

class Tools{

    public function issetPerson($repostitory, $parameters = array()){

    }

    public function explodePersonName($full_name){
        $name_array = explode(" ", trim($full_name));

        $person_name = trim($name_array[0]);

        $person_last_name = trim(str_replace($person_name, "", $full_name));

        return array("name" => $person_name, "last_name" => $person_last_name);
    }

    public function getCountryCodeFromCountryName($country_name){
        $countries = Intl::getRegionBundle()->getCountryNames();

        foreach ($countries as $key => $value){
            if(strtolower($value) == strtolower($country_name)){
                $country_code = $key;
                break;
            }
        }

        return $country_code;
    }

    public function cleanStringBetweenTags($string, $start_tag, $end_tag, $escape_tags = true){
        if($escape_tags){
            $start_tag = "\\" . $start_tag;
            $end_tag = "\\". $end_tag;
        }
        preg_match_all("/.*($start_tag(.*)$end_tag).*/", $string,$matches);

        if(isset($matches[1][0])){
            $string = str_replace($matches[1][0], "", $string);
        }

        return $string;
    }

    public function contains($haystack, $needle, $case_sensitive = false){
        if($case_sensitive){
            $return = strpos(strtolower($haystack), strtolower($needle));
        } else{
            $return = strpos($haystack, $needle);
        }

        return $return;
    }

    public function make_call($url){
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }
}